//
//  ViewController.swift
//  SimpsonsApp
//
//  Created by Greg on 2019-05-04.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Cocoa
import AVKit

class RoohButtonCell : NSCell
{
	required init(coder: NSCoder)
	{
		super.init()
	}
}

class MainViewController: NSViewController, XMLParserDelegate, NSTableViewDataSource, NSTableViewDelegate
{
	@IBOutlet var theTable:NSTableView!
	@IBOutlet var columnPlayButton:NSTableColumn!
	@IBOutlet var columnSeason:NSTableColumn!
	@IBOutlet var columnEpisodeNum:NSTableColumn!
	@IBOutlet var columnTitle:NSTableColumn!
	@IBOutlet var columnDesc:NSTableColumn!
	@IBOutlet var columnFilename:NSTableColumn!
	@IBOutlet var columnNewFilename:NSTableColumn!
	@IBOutlet var columnFileStatus:NSTableColumn!
    @IBOutlet var columnMachineLearningStatus:NSTableColumn!
    @IBOutlet var columnHomerPercent:NSTableColumn!
    @IBOutlet var columnMargePercent:NSTableColumn!
    @IBOutlet var columnBartPercent:NSTableColumn!
    @IBOutlet var columnLisaPercent:NSTableColumn!
	@IBOutlet var videoplayer:AVPlayerView!
	@IBOutlet var previewFrame:NSImageView!
	var playButton:NSButtonCell!

	var fullEpisodeList = Array<EpisodeAttributes>()
	
	var currentElement = ""
	var currentSeason = ""
	var currentEpisodeNumOfSeason = 0
	var currentTitle = ""
	var currentDesc = ""
	var currentFilename = ""
	
	override func viewDidLoad()
	{
		super.viewDidLoad()
		
		if let videoPlayer = self.videoplayer {
			videoPlayer.isHidden = true
		}
		
		if let xmlPath = Bundle.main.path(forResource: "Simpsons", ofType: "xml") {
			let xmlPathURL = URL.init(fileURLWithPath:xmlPath)
			
			do {
				let xmlData = try Data.init(contentsOf:xmlPathURL)
				let xmlParser = XMLParser.init(data:xmlData)
				xmlParser.delegate = self
				xmlParser.parse()
			}catch{
				
			}
		}
	}
	
	@IBAction func menucommandRename(sender:AnyObject)
	{
		self.performSegue(withIdentifier: "rename_dialog", sender: self)
	}
	
    @IBAction func menucommandMachineLearning(sender:AnyObject)
    {
        self.performSegue(withIdentifier: "machineLearning_dialog", sender: self)
    }

    @IBAction func menucommandOneGiantMovie(sender:AnyObject)
    {
        self.performSegue(withIdentifier: "oneGiantMovie_dialog", sender: self)
    }
    
    @IBAction func menucommandConvertSummaryCSVsIntoOneCSV(sender:AnyObject)
    {
        self.performSegue(withIdentifier: "summaryCSVsIntoOneCSV", sender: self)
    }

    override func prepare(for segue: NSStoryboardSegue, sender: Any?)
	{
		super.prepare(for:segue, sender:sender)
		
		if segue.identifier == "rename_dialog" {
			if let vc = segue.destinationController as? RenameViewController {
				vc.fullEpisodeList = self.fullEpisodeList
			}
		}
		if segue.identifier == "machineLearning_dialog" {
			if let vc = segue.destinationController as? MachineLearningViewController {
				vc.fullEpisodeList = self.fullEpisodeList
			}
		}
        if segue.identifier == "oneGiantMovie_dialog" {
            if let vc = segue.destinationController as? OneGiantMovieViewController {
                vc.fullEpisodeList = self.fullEpisodeList
            }
        }
        if segue.identifier == "summaryCSVsIntoOneCSV" {
            if let vc = segue.destinationController as? SummaryCSVsIntoOneCSVViewController {
                vc.fullEpisodeList = self.fullEpisodeList
            }
        }
    }
	
	// MARK: - XML Parser delegate methods
	
	func parserDidStartDocument(_ parser: XMLParser)
	{
		print("XML parser started")
	}
	
	func parserDidEndDocument(_ parser: XMLParser)
	{
		print("XML parser ended")
		theTable.reloadData()
	}
	
	func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:])
	{
		currentElement = elementName

		if currentElement == "Season" {
			currentEpisodeNumOfSeason = 0
		}
		
		if currentElement == "Episode" {
			currentTitle = ""
			currentDesc = ""
			currentFilename = ""
		}
	}

	func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?)
	{
		// we're at the end of an element, so no more "current element"
		currentElement = ""
		
		if elementName == "Episode" {
			let pAttributes = EpisodeAttributes()
			pAttributes.season = self.currentSeason
			pAttributes.title = self.currentTitle
			pAttributes.desc = self.currentDesc
			pAttributes.xmlFilename = self.currentFilename
			
			self.currentEpisodeNumOfSeason = self.currentEpisodeNumOfSeason + 1
			pAttributes.episodeNumOfSeason = self.currentEpisodeNumOfSeason

			fullEpisodeList.append(pAttributes)
		}
	}
	
	// The XML paresr calls this to report what it finds between any XML tags
	func parser(_ parser: XMLParser, foundCharacters string: String)
	{
		if currentElement == "Season" {
			if let _ = Int(string) {
				currentSeason = string
			}
		}
		if currentElement == "Title" {
			currentTitle = string
		}
		if currentElement == "Desc" {
			currentDesc = string
		}
		if currentElement == "Filename" {
			currentFilename = string
		}
	}
	
	// MARK: - NSTableView delegate methods
	
	func numberOfRows(in tableView: NSTableView) -> Int
	{
		return fullEpisodeList.count
	}
	
	@objc func playButtonClicked(sender:NSButton)
	{
		print("X")
	}
	
	func tableView(_ tableView: NSTableView, objectValueFor tableColumn: NSTableColumn?, row: Int) -> Any?
	{
		let episodeInfo = self.fullEpisodeList[row]
		
		if tableColumn == columnPlayButton {
			if self.playButton == nil {
				self.playButton = NSButtonCell.init(textCell:"X")
				self.playButton.title = "abc"
				self.playButton.setButtonType(.pushOnPushOff)
				self.playButton.action = #selector(playButtonClicked(sender:))
				self.playButton.controlView?.frame = NSRect(x: 0, y: 0, width: 40, height: 40)
			}
			return self.playButton
		}
		if tableColumn == columnSeason {
			return episodeInfo.season
		}
		if tableColumn == columnEpisodeNum {
			return "\(episodeInfo.episodeNumOfSeason)"
		}
		if tableColumn == columnTitle {
			return episodeInfo.title
		}
		if tableColumn == columnDesc {
			return episodeInfo.desc
		}
		if tableColumn == columnFilename {
			return episodeInfo.existingFilename()
		}
		if tableColumn == columnNewFilename {
			return episodeInfo.newFilenameWanted()
		}
		if tableColumn == columnFileStatus {
			return episodeInfo.filenameStatus()
		}
        if tableColumn == columnMachineLearningStatus {
            return episodeInfo.machineLearningStatus()
        }
        if tableColumn == columnHomerPercent {
            return episodeInfo.machineLearningPercentageForCharacter("homer_simpson")
        }
        if tableColumn == columnMargePercent {
            return episodeInfo.machineLearningPercentageForCharacter("marge_simpson")
        }
        if tableColumn == columnBartPercent {
            return episodeInfo.machineLearningPercentageForCharacter("bart_simpson")
        }
        if tableColumn == columnLisaPercent {
            return episodeInfo.machineLearningPercentageForCharacter("lisa_simpson")
        }

		return "Fooh"	// should not get here
	}
}

