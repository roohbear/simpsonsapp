//
//  summaryCSVsIntoOneCSVViewController.swift
//  SimpsonsApp
//
//  Created by G Bear on 2020-01-09.
//  Copyright © 2020 RoohBear. All rights reserved.
//

import Foundation
import Cocoa

class SummaryCSVsIntoOneCSVViewController : NSViewController
{
    @IBOutlet var progressBar:NSProgressIndicator!
    var fullEpisodeList:Array<EpisodeAttributes>!

    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        if let layer = self.progressBar.layer {
            layer.transform = CATransform3DMakeScale(5.0, 5.0, 1.0)
        }
    }
    
    @IBAction func buttonGoClicked(sender:AnyObject)
    {
        self.progressBar.doubleValue = 0
        self.progressBar.maxValue = 1.0
        
        var episodeCount = 0
        for episode in self.fullEpisodeList {
            episode.readSummaryCSVFile()
            
            episodeCount += 1
            self.progressBar.doubleValue = Double(episodeCount / fullEpisodeList.count)
        }

        let newSummaryCSVFilename = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSV.csv"

        let fm = FileManager.init()
        if fm.fileExists(atPath:newSummaryCSVFilename) {
            do {
                try fm.removeItem(atPath:newSummaryCSVFilename)
            }catch{
            }
        }

        var line = "episode,title,description"
        if let firstEpisode = self.fullEpisodeList.first {
            for actor in firstEpisode.mlSummary {
                if line.count > 0 {
                    line += ","
                }

                line += "\(actor.name)"
            }
            line += "\r\n"
        }
        var output = line
        
        for episode in self.fullEpisodeList {
            let titleNoQuotes = episode.title.replacingOccurrences(of:"\"", with:"'")
            let descNoQuotes = episode.desc.replacingOccurrences(of:"\"", with:"'")
            var line = "s\(episode.twoCharSeason())e\(episode.twoCharEpisode()),\"\(titleNoQuotes)\",\"\(descNoQuotes)\""
            
            if let mlSummary = episode.mlSummary {
                for actor in mlSummary {
                    if line.count > 0 {
                        line += ","
                    }

                    line += "\(actor.appearanceValue)"
                }
                line += "\r\n"
                output += line
            }
        }

        output = output.replacingOccurrences(of:"nan", with:"0")
        fm.createFile(atPath:newSummaryCSVFilename, contents:output.data(using: .utf8, allowLossyConversion:true), attributes:nil)
        self.progressBar.doubleValue = 1.0
    }
    
}
