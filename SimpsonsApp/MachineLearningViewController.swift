//
//  MachineLearningViewController.swift
//  SimpsonsApp
//
//  Created by Greg on 2019-10-06.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Foundation
import Cocoa


class MachineLearningViewController : NSViewController
{
	@IBOutlet var scrollView:NSScrollView!
	var operationQ = OperationQueue.init()
	var fullEpisodeList:Array<EpisodeAttributes>!
	
	override func viewDidAppear()
	{
		super.viewDidAppear()
		
		let WIDTH = 320 / 8
		let HEIGHT = 240 / 8
		var x = 0
		var y = 0

        operationQ.maxConcurrentOperationCount = 1

		for videoNum in 0..<self.fullEpisodeList.count {
			let imageview = NSImageView.init(frame:NSRect.init(x:x, y:y, width:WIDTH, height:HEIGHT))
			self.scrollView.addSubview(imageview)
            
			let episode = self.fullEpisodeList[videoNum]
			if let existingFilename = episode.existingFilenameWithPath() {
				let episodeURL = URL.init(fileURLWithPath:existingFilename)
	
				let operation = ProcessVideoOperation.init()
				operation.episodeURL = episodeURL
				operation.imageView = imageview
				operationQ.addOperation(operation)
			}
			
			x += WIDTH
			if x > 1000 {
				x = 0
				y += HEIGHT - 5
			}
		}
	}
	
	@IBAction func stopClicked(sender:NSButton)
	{
		operationQ.cancelAllOperations()
	}
	
	override func viewDidDisappear()
	{
		operationQ.cancelAllOperations()
	}
}
