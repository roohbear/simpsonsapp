//
//  EpisodeAttribues.swift
//  SimpsonsApp
//
//  Created by Greg on 2019-05-05.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Foundation


// class for retaining the popularity of an actor
class ActorInfo
{
    var name = ""                   // actor name
    var appearanceValue = 0.0       // percentage found in the episode
}

class EpisodeAttributes : NSObject
{
	var season = ""
	var episodeNumOfSeason = 0
	var title = ""
	var desc = ""
	var xmlFilename = ""
    var mlSummary:[ActorInfo]!
    
	func twoCharSeason() -> String
	{
		var strSeason = "\(self.season)"
		if strSeason.count == 1 {
			strSeason = "0\(self.season)"
		}
		return strSeason
	}
	
	func twoCharEpisode() -> String
	{
		var strEpisode = "\(self.episodeNumOfSeason)"
		if strEpisode.count == 1 {
			strEpisode = "0\(self.episodeNumOfSeason)"
		}
		return strEpisode
	}
	
	// returns the new filename wanted (for the rename operation)
	func newFilenameWanted() -> String?
	{
		var suffix = ""
		if let existingFilename = self.existingFilename() {
			if existingFilename.hasSuffix(".mov") {
				suffix = "mov"
			}
			if existingFilename.hasSuffix(".mp4") {
				suffix = "mp4"
			}
			if existingFilename.hasSuffix(".m4v") {
				suffix = "m4v"
			}
			
			if self.xmlFilename.count > 0 {
				return "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).\(suffix)"
			}else{
				return "s\(self.twoCharSeason())e\(self.twoCharEpisode()).\(suffix)"
			}
		}
		
		return nil
	}
    
    // returns the filename found for an existing episode
	func existingFilename() -> String?
	{
		var ret = self.xmlFilename
		if self.fileExists(filename:ret) {
			return ret
		}
		
		ret = "\(self.xmlFilename).m4v"
		if self.fileExists(filename:ret) {
			return ret
		}
		
		ret = "\(self.xmlFilename).mp4"
		if self.fileExists(filename:ret) {
			return ret
		}
		
		ret = "\(self.xmlFilename).mov"
		if self.fileExists(filename:ret) {
			return ret
		}
		
		ret = "\(self.xmlFilename) (Converted).mov"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()).m4v"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()).mp4"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).mp4"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "S\(self.twoCharSeason())E\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).m4v"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).m4v"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).mp4"
		if self.fileExists(filename:ret) {
			return ret
		}

		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).mov"
		if self.fileExists(filename:ret) {
			return ret
		}
		
		ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename) (Converted).mov"
		if self.fileExists(filename:ret) {
			return ret
		}
		
		return nil
	}
	
	// returns true if the filename passed in exists on either 8TB or in my Documents folder
	func fileExists(filename:String) -> Bool
	{
		let fm = FileManager.init()

		var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/Episodes/\(filename)"
		if(fm.fileExists(atPath:filePath)) {
			return true
		}
	
        filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/episodes/\(filename)"
        if(fm.fileExists(atPath:filePath)) {
            return true
        }
		
		return false
	}
    
	// looks for the movie file and returns either "Ok" or "Missing"
	func filenameStatus() -> String
	{
		if let _ = self.existingFilename() {
			return "Ok"
		}else{
			return "Missing"
		}
	}

	func existingFilenameWithPath() -> String?
	{
		let fm = FileManager.init()
		
		if let existingFilename = self.existingFilename() {
			var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/Episodes/\(existingFilename)"
			if(fm.fileExists(atPath:filePath)) {
				return filePath
			}
		
            filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/episodes/\(existingFilename)"
            if(fm.fileExists(atPath:filePath)) {
                return filePath
            }
		}
		
		return nil
	}
    
	func newFilenameWithPath() -> String?
	{
		if let wanted = self.newFilenameWanted() {
			if wanted.count > 0 {
				let filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/episodes/\(wanted)"
				return filePath
			}
		}
		
		return nil
	}
    
    // MARK: - Machine Learning stuff
    
    // returns the CSV filename found
    func csvExistingFilename() -> String?
    {
        var ret = self.xmlFilename
        if self.csvFileExists(filename:ret) {
            return ret
        }
        
        ret = "\(self.xmlFilename).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }
        
        ret = "\(self.xmlFilename) (Converted).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }

        ret = "S\(self.twoCharSeason())E\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }
        
        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename) (Converted).csv"
        if self.csvFileExists(filename:ret) {
            return ret
        }
        
        return nil
    }
    
    // returns the CSV filename found
    func csvSummaryExistingFilename() -> String?
    {
        var ret = self.xmlFilename
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }
        
        ret = "\(self.xmlFilename).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }
        
        ret = "\(self.xmlFilename) (Converted).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }

        ret = "S\(self.twoCharSeason())E\(self.twoCharEpisode()) - \(self.xmlFilename.replacingOccurrences(of:" ", with:".")).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }

        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }
        
        ret = "s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename) (Converted).csv"
        if self.csvSummaryFileExists(filename:ret) {
            return ret
        }
        
        return nil
    }

    // returns true if the CSV filename passed in exists
    func csvFileExists(filename:String) -> Bool
    {
        let fm = FileManager.init()

        var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/CSVs/\(filename)"
        if(fm.fileExists(atPath:filePath)) {
            return true
        }

        filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/CSVs/\(filename)"
        if(fm.fileExists(atPath:filePath)) {
            return true
        }

        return false
    }
    
    func csvSummaryFileExists(filename:String) -> Bool
    {
        let fm = FileManager.init()

        var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/SummaryCSVs/\(filename)"
        if(fm.fileExists(atPath:filePath)) {
            return true
        }

        filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSVs/\(filename)"
        if(fm.fileExists(atPath:filePath)) {
            return true
        }

        return false
    }

    func csvExistingFilenameWithPath() -> String?
    {
        let fm = FileManager.init()
        
        if let csvExistingFilename = self.csvExistingFilename() {
            var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/CSVs/\(csvExistingFilename)"
            if(fm.fileExists(atPath:filePath)) {
                return filePath
            }
        
            filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/CSVs/\(csvExistingFilename)"
            if(fm.fileExists(atPath:filePath)) {
                return filePath
            }
        }
        
        return nil
    }
    
    func csvSummaryExistingFilenameWithPath() -> String?
    {
        let fm = FileManager.init()
        
        if let csvExistingFilename = self.csvSummaryExistingFilename() {
            var filePath = "/Volumes/8TB/Videos/Toons/Simpsons/SummaryCSVs/\(csvExistingFilename)"
            if(fm.fileExists(atPath:filePath)) {
                return filePath
            }
        
            filePath = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSVs/\(csvExistingFilename)"
            if(fm.fileExists(atPath:filePath)) {
                return filePath
            }
        }
        
        return nil
    }

    // looks for the .CSV file for the episode and returns either "Ok" or "Missing"
    func machineLearningStatus() -> String
    {
        if let _ = self.csvExistingFilename() {
            return "Ok"
        }else{
            return "Missing"
        }
    }
    
    // Returns a string showing the % of appearances in the episode.
    // characterName must be one of the character names in the CSV file
    // returns "?" if character name not found.
    // returns "Unknown" if CSV file not found.
    func machineLearningPercentageForCharacter(_ characterName:String) -> String
    {
        if let csvFilename = self.csvExistingFilenameWithPath() {
            if let csvSummaryFilename = self.csvSummaryExistingFilename() {
                // use the summary CSV file
                return self.readSummaryCSVFile(characterName)
            }else{
                // make a summary CSV file then read it
                self.makeSummaryCSVFile()
                return self.readSummaryCSVFile(characterName)
            }
        }

        return "Unknown"
    }
    
    func readSummaryCSVFile(_ characterName:String) -> String
    {
        let summaryCSVFilename = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSVs/s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).csv"

        if let csvData = FileManager.default.contents(atPath:summaryCSVFilename) {
            let csvString = String.init(data:csvData, encoding:.utf8)
            if var arrCSVLines = csvString?.components(separatedBy: "\n") {
                arrCSVLines.removeLast()
                
                for csvLine:String in arrCSVLines {
                    var arrValues = csvLine.components(separatedBy:",")
                    if let characterForLine = arrValues.first {
                        if characterName == characterForLine {
                            arrValues.removeFirst()
                            if let firstValue = arrValues.first {
                                let firstValueClean = firstValue.replacingOccurrences(of:"\r", with:"")
                                return firstValueClean
                            }
                        }
                    }
                }
            }
        }
        
        return "0"
    }
    
    func readSummaryCSVFile()
    {
        let summaryCSVFilename = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSVs/s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).csv"

        self.mlSummary = nil
        
        if let csvData = FileManager.default.contents(atPath:summaryCSVFilename) {
            let csvString = String.init(data:csvData, encoding:.utf8)
            if var arrCSVLines = csvString?.components(separatedBy: "\n") {
                arrCSVLines.removeLast()

                self.mlSummary = [ActorInfo]()
                
                for csvLine:String in arrCSVLines {
                    let actorInfo = ActorInfo()

                    var arrValues = csvLine.components(separatedBy:",")
                    if let characterForLine = arrValues.first {
                        actorInfo.name = characterForLine

                        arrValues.removeFirst()
                        if let firstValue = arrValues.first {
                            let firstValueClean = firstValue.replacingOccurrences(of:"\r", with:"")
                            if let dblFirstValueClean = Double(firstValueClean) {
                                actorInfo.appearanceValue = dblFirstValueClean
                            }
                        }

                        self.mlSummary.append(actorInfo)
                    }
                }
            }
        }
    }

    func makeSummaryCSVFile()
    {
        let newSummaryCSVFilename = "/Users/gbear/Documents/8TB/Videos/Toons/Simpsons/SummaryCSVs/s\(self.twoCharSeason())e\(self.twoCharEpisode()) - \(self.xmlFilename).csv"

        var output = ""
        let fm = FileManager.init()
        if fm.fileExists(atPath:newSummaryCSVFilename) {
            do {
                try fm.removeItem(atPath:newSummaryCSVFilename)
            }catch{
            }
        }
        
        if let csvFilename = self.csvExistingFilenameWithPath() {
            if let csvData = FileManager.default.contents(atPath:csvFilename) {
                let csvString = String.init(data:csvData, encoding:.utf8)
                if var arrCSVLines = csvString?.components(separatedBy: "\n") {
                    arrCSVLines.removeLast()
                    
                    for csvLine:String in arrCSVLines {
                        var arrValues = csvLine.components(separatedBy:",")
                        if let characterName = arrValues.first {
                            arrValues.removeFirst()

                            var total = 0.0
                            for value in arrValues {
                                if let valueAsDouble = Double(value) {
                                    total += valueAsDouble
                                }
                            }

                            output += "\(characterName),\((total / Double(arrValues.count) * 100))\r\n"
                        }
                    }
                }
            }
        }

        fm.createFile(atPath:newSummaryCSVFilename, contents:output.data(using: .utf8, allowLossyConversion:true), attributes:nil)
    }
}

