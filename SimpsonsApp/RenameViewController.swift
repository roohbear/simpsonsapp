//
//  RenameViewController.swift
//  SimpsonsApp
//
//  Created by Greg on 2019-05-04.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Foundation
import Cocoa


class RenameViewController : NSViewController
{
	@IBOutlet var buttonSimulate:NSButton!
	var fullEpisodeList:Array<EpisodeAttributes>!
	
	@IBAction func buttonGoClicked(sender:NSButton)
	{
		let fm = FileManager.init()
		
		for episode in fullEpisodeList {
			if let from = episode.existingFilenameWithPath() {
				if let to = episode.newFilenameWithPath() {
					if from.count > 0 && to.count > 0 {
						if from != to {
							if buttonSimulate.state == .on {
								print("Would rename \(from) to \(to)")
							}else{
								do {
									let _ = try fm.moveItem(atPath:from, toPath:to)
								}catch let error {
									print("error \(error) renaming \(from) to \(to)")
								}
							}
						}
					}
				}
			}
		}
	}
}

