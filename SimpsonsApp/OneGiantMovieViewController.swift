//
//  OneGiantMovieViewController.swift
//  SimpsonsApp
//
//  Created by G Bear on 2019-11-24.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Foundation
import Cocoa
import AVKit

class OneGiantMovieViewController : NSViewController
{
    @IBOutlet var numRows:NSTextField!
    @IBOutlet var numColumns:NSTextField!
    @IBOutlet var numFromFile:NSTextField!
    @IBOutlet var numToFile:NSTextField!
    @IBOutlet var numSizeX:NSTextField!
    @IBOutlet var numSizeY:NSTextField!
    
    @IBOutlet var contentView:NSView!
    @IBOutlet var sliderFrom:NSSlider!
    @IBOutlet var sliderTo:NSSlider!

    var operationQ = OperationQueue.init()
    var fullEpisodeList:Array<EpisodeAttributes>!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        //operationQ.maxConcurrentOperationCount = 4
        
        if let x = numRows {
            x.stringValue = "1"
        }
        if let x = numColumns {
            x.stringValue = "31"
        }
        if let x = numFromFile {
            x.stringValue = "1"
        }
        if let x = numToFile {
            x.stringValue = "\(fullEpisodeList.count)"
        }
        if let x = numSizeX {
            x.stringValue = "64"
        }
        if let x = numSizeY {
            x.stringValue = "40"
        }
        if let x = sliderFrom {
            x.minValue = 0
            x.maxValue = 24 * 60   // 24 minutes
            x.intValue = 11 * 60   // 11 minute-mark
        }
        if let x = sliderTo {
            x.minValue = 0
            x.maxValue = 24 * 60   // 24 minutes
            x.intValue = 12 * 60   // 12 minute-mark
        }
        
        self.makeImageViews()
    }
    
    func makeImageViews()
    {
        guard let contentView = self.contentView else {return}
        contentView.subviews.removeAll()
        contentView.translateOrigin(to:NSPoint.init(x: 0, y: 0))
        
        guard let fromFileField = self.numFromFile else {return}
        guard let nFromFile = Int32(fromFileField.stringValue) else {return}

        guard let toFileField = self.numToFile else {return}
        guard let nToFile = Int32(toFileField.stringValue) else {return}

        guard let numColumnsField = self.numColumns else {return}
        guard let nColumns = Int32(numColumnsField.stringValue) else {return}
        
        guard let widthField = self.numSizeX else {return}
        guard let width = Int32(widthField.stringValue) else {return}
        
        guard let heightField = self.numSizeY else {return}
        guard let height = Int32(heightField.stringValue) else {return}
        
        var column = 0
        var x:Int32 = 0
        var y:Int32 = (nToFile / nColumns) * height

        for nFile in nFromFile ..< nToFile {
            let rc = CGRect.init(x:CGFloat(x), y:CGFloat(y), width:CGFloat(width), height:CGFloat(height))
            let imageView = NSImageView.init(frame:rc)
            imageView.tag = Int(nFile)
            contentView.addSubview(imageView)
            
            column += 1
            x += width
            if column == nColumns {
                column = 0
                x = 0
                
                y -= height
            }
        }
    }

    // this gets called if the user adjusts either slider
    @IBAction func sliderChanged(sender:NSSlider)
    {
        self.makeImageViews()
        getFrames(timeToGet:sender.floatValue, frameNum:0)
    }
    
    @IBAction func buttonGoClicked(sender:NSButton)
    {
        self.makeImageViews()

        DispatchQueue.global(qos: .background).async {
            
            if let safeSliderFrom = self.sliderFrom {
                if let safeSliderTo = self.sliderTo {
                    for range in safeSliderFrom.intValue...safeSliderTo.intValue {

                        for frame in 0...30 {
                            print("Getting frames for time \(range), frame \(frame)")
                            self.getFrames(timeToGet:Float(range), frameNum:frame)
                            self.operationQ.waitUntilAllOperationsAreFinished()
                        }
                    }
                }
            }

        }
        
    }

    // gets the frames from all videos specified by the UI and adds them to the scrollview
    func getFrames(timeToGet:Float, frameNum:Int)
    {
        guard let fromFileField = self.numFromFile else {return}
        guard let nFromFile = Int32(fromFileField.stringValue) else {return}

        guard let toFileField = self.numToFile else {return}
        guard let nToFile = Int32(toFileField.stringValue) else {return}
        
        guard let episodeList = self.fullEpisodeList else {return}

        for nFile in nFromFile ..< nToFile {
            let episodeIndex = episodeList.index(Int(Int32(nFile - 1)), offsetBy: 0)
            let episodeX = episodeList[episodeIndex]
            if let existingFilename = episodeX.existingFilenameWithPath() {
                // make an operation object that will open the source file, get the frame and set it in the imageview
                let op = GetFrameOperation.init()
                op.contentView = self.contentView
                op.imageViewTag = Int(nFile)
                op.timeToGet = timeToGet
                op.frameNum = frameNum
                op.episodeURL = URL.init(fileURLWithPath:existingFilename)
                operationQ.addOperation(op)
            }
        }
    }
}

class GetFrameOperation : Operation
{
    var contentView:NSView!
    var imageViewTag:Int = 0
    var episodeURL:URL!
    var timeToGet:Float = 0.0
    var frameNum = 0
    
    override func main()
    {
        let playerItem = AVAsset.init(url:self.episodeURL)
        let generateImg = AVAssetImageGenerator.init(asset:playerItem)
        generateImg.requestedTimeToleranceBefore = CMTime.zero;
        generateImg.requestedTimeToleranceAfter = CMTime.zero;
        let fTimeToGet = timeToGet + (Float(frameNum) / 30.0)

        do {
            let image = try generateImg.copyCGImage(at:CMTime.init(seconds:Float64(fTimeToGet), preferredTimescale:Int32(NSEC_PER_SEC)), actualTime:nil)
            let nsImage = NSImage.init(cgImage:image, size:NSSize.init(width:image.width, height:image.height))

            DispatchQueue.main.async {
                if let imageView = self.contentView.viewWithTag(self.imageViewTag) as? NSImageView {
                    imageView.image = nsImage
                }
            }
        }catch{
            print("unable to get frame for \(String(describing: self.episodeURL))")
        }
    }
    
    override func cancel()
    {
    }
}


