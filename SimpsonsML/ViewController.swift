//
//  ViewController.swift
//  SimpsonsML
//
//  Created by G Bear on 2019-12-31.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import UIKit
import AVKit
import MobileCoreServices

enum PickerType:Int
{
    case Unknown
    case SourcePicker
    case DestPicker
}

class ViewController: UIViewController, UIDocumentPickerDelegate, UIDocumentBrowserViewControllerDelegate
{
    @IBOutlet var labelSource:UILabel!
    @IBOutlet var labelDest:UILabel!
    @IBOutlet var viewContainer:UIView!
    
    var pickerUsage:PickerType = .Unknown
    var urlSourceFolder:URL!
    var urlDestFolder:URL!
    var shouldStopAccessingURL = false
    var urlFiles = [URL]()
    var picker:UIDocumentPickerViewController!
    var operationQ = OperationQueue.init()
    
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
    
        operationQ.maxConcurrentOperationCount = 4
    }
    
    @IBAction func sourceFolderClicked(sender:UIButton)
    {
        self.pickerUsage = .SourcePicker
        
        picker = UIDocumentPickerViewController(documentTypes:[kUTTypeFolder as String], in:.open)
        picker.delegate = self
        present(picker, animated:true, completion:nil)
    }
    
    @IBAction func destFolderClicked(sender:UIButton)
    {
        self.pickerUsage = .DestPicker
        
        picker = UIDocumentPickerViewController(documentTypes:[kUTTypeFolder as String], in:.open)
        picker.delegate = self
        present(picker, animated:true, completion:nil)
    }
    
    @IBAction func goButtonClicked(sender:UIButton)
    {
        var iv = 1
        for videoNum in 0..<self.urlFiles.count {
            let episodeURL = self.urlFiles[videoNum]

            let operation = ProcessVideoOperation.init()
            operation.episodeURL = episodeURL
            operation.imageView = self.viewContainer.viewWithTag(iv) as? UIImageView
            operation.labelEpisode = self.viewContainer.viewWithTag(iv + 10) as? UILabel
            operationQ.addOperation(operation)
            
            iv = iv + 1
            if iv > 9 {
                iv = 1
            }
        }
    }

    func documentPicker(_ controller:UIDocumentPickerViewController, didPickDocumentsAt urls:[URL])
    {
        if let urlPickedFolder = urls.first {
            if self.pickerUsage == .SourcePicker {
                self.urlSourceFolder = urlPickedFolder
                self.labelSource.text = urlPickedFolder.absoluteString
            }
            if self.pickerUsage == .DestPicker {
                self.urlDestFolder = urlPickedFolder
                self.labelDest.text = urlPickedFolder.absoluteString
            }

            // must do this startAccessingSecurityScopedResource call and match it with a stopAccessingSecurityScopedResource call when done
            self.shouldStopAccessingURL = urlPickedFolder.startAccessingSecurityScopedResource()

            // actually get the list of files
            if self.pickerUsage == .SourcePicker {
                urlFiles.removeAll()
                var coordinatedError:NSError!
                NSFileCoordinator().coordinate(readingItemAt:urlPickedFolder, error:&coordinatedError) { (folderURL) in
                    do {
                        let keys : [URLResourceKey] = [URLResourceKey.nameKey, URLResourceKey.isDirectoryKey, URLResourceKey.fileSizeKey]
                        let filelist = FileManager.default.enumerator(at:urlPickedFolder, includingPropertiesForKeys:keys)
                        for case let file as URL in filelist! {
                            urlFiles.append(file)
                        }
                    }
                }
                
                // remove the first URL then sort the URLs alphabetically in descending order
                urlFiles.removeFirst()
                urlFiles.sort { (url1:URL, url2:URL) -> Bool in
                    if url1.absoluteString.compare(url2.absoluteString) == .orderedAscending {
                        return false
                    }
                    return true
                }
            }
        }
    }
}

