//
//  ContentView.swift
//  SimpsonsPad
//
//  Created by G Bear on 2019-12-31.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import SwiftUI

struct ContentView: View
{
    var urlSourceFolder:URL!
    var urlDestinationFolder:URL!

    var body: some View
    {
        Button(action: {
            print("clicked")
        })
        {
        Text("Source folder")
        }
    }
    
}

struct ContentView_Previews: PreviewProvider
{
    static var previews: some View {
        ContentView()
    }
}
