//
//  ProcessVideoOperation.swift
//  SimpsonsApp
//
//  Created by G Bear on 2020-01-01.
//  Copyright © 2020 RoohBear. All rights reserved.
//

import Foundation
import AVKit
import CoreML

class ProcessVideoOperation : Operation
{
    var episodeURL:URL!
    #if os(macOS)
        var labelEpisode:NSTextField!
        var imageView:NSImageView!
    #endif
    #if os(iOS)
        var labelEpisode:UILabel!
        var imageView:UIImageView!
    #endif
    
    var bCancel = false
    var bDone = false
    var pathForCSVWithExtension:URL!
    var arrCSV = [String : [Double]]()
    var machineLearningModel = SimpsonsClassifier()

    override func start()
    {
        let pathForCSV = self.episodeURL.absoluteString
        var strPathForCSVWithExtension = String(pathForCSV.prefix(pathForCSV.count - 3) + "csv")
        strPathForCSVWithExtension = strPathForCSVWithExtension.replacingOccurrences(of:"file://", with:"")
        strPathForCSVWithExtension = strPathForCSVWithExtension.replacingOccurrences(of:"%20", with:" ")
        strPathForCSVWithExtension = strPathForCSVWithExtension.replacingOccurrences(of:"/episodes/", with:"/CSVs/")  // works if using SSD
        strPathForCSVWithExtension = strPathForCSVWithExtension.replacingOccurrences(of:"/Episodes/", with:"/CSVs/")  // works if using 8TB
        self.pathForCSVWithExtension = URL.init(fileURLWithPath:strPathForCSVWithExtension)
        
        // if CSV file already exists, return now
        let fm = FileManager.init()
        if fm.fileExists(atPath:strPathForCSVWithExtension) {
            self.bDone = true
            print("skipping \(String(describing: self.episodeURL.lastPathComponent))")
            return
        }

        var frameCounter = 0
        let playerItem = AVAsset.init(url:self.episodeURL)
        
        do {
            let reader = try AVAssetReader(asset:playerItem)
            if let videoTrack = playerItem.tracks(withMediaType:.video).first {
                
                // read video frames as BGRA
                let trackReaderOutput = AVAssetReaderTrackOutput(track:videoTrack, outputSettings:[String(kCVPixelBufferPixelFormatTypeKey): NSNumber(value: kCVPixelFormatType_32BGRA)])
                reader.add(trackReaderOutput)
                reader.startReading()
                print("starting processing of \(String(describing: self.episodeURL.lastPathComponent))")

                DispatchQueue.main.async {
                    #if os(macOS)
                        if let label = self.labelEpisode {
                            label.stringValue = self.episodeURL.lastPathComponent
                        }
                    #endif
                    #if os(iOS)
                        self.labelEpisode.text = self.episodeURL.lastPathComponent
                    #endif
                }
                
                while let sampleBuffer = trackReaderOutput.copyNextSampleBuffer() {
                    if bCancel == true {
                        break;
                    }
                    
                    if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
                        let machineModelInput = SimpsonsClassifierInput.init(image:imageBuffer)
                        guard let prediction = try? machineLearningModel.prediction(input:machineModelInput) else {
                            fatalError("Unexpected runtime error.")
                        }

                        for character in prediction.classLabelProbs {
                            let characterKey = character.key
                            let characterValue = character.value

                            // create array if needed
                            if let _ = arrCSV[characterKey] {
                                // append to array
                                arrCSV[characterKey]?.append(characterValue)
                            }else{
                                arrCSV[characterKey] = [Double(characterValue)]
                            }
                        }
                        
                        frameCounter += 1
                        if frameCounter == 500 {
                            frameCounter = 0
                            
                            DispatchQueue.main.async {
                                let ciImage = CIImage(cvImageBuffer:imageBuffer)
                                let context = CIContext(options:nil)
                                let width = CVPixelBufferGetWidth(imageBuffer)
                                let height = CVPixelBufferGetHeight(imageBuffer)
                                let cgImage = context.createCGImage(ciImage, from: CGRect(x: 0, y: 0, width: width, height: height))
                                
                                #if os(macOS)
                                    let nsImage = NSImage(cgImage: cgImage!, size: CGSize(width: width, height: height))
                                #endif
                                #if os(iOS)
                                    let nsImage = UIImage(cgImage:cgImage!)
                                #endif
                            
                                self.imageView.image = nsImage
                            }
                        }
                    }
                }
                
                self.writeCSVFile()
            }
        }catch{
            print("cannot make AVAssetReader for \(String(describing: self.episodeURL))")
        }

        DispatchQueue.main.async {
            self.imageView.image = nil

            #if os(macOS)
                if let label = self.labelEpisode {
                    label.stringValue = ""
                }
            #endif
            #if os(iOS)
                self.labelEpisode.text = ""
            #endif
        }

        self.bDone = true
    }
    
    func writeCSVFile()
    {
        if arrCSV.keys.isEmpty {
            return
        }
        
        // write the contents of arrCSV to a .csv file
        var unsortedKeys = [String]()
        for key in arrCSV.keys {
            unsortedKeys.append(key)
        }
        let sortedKeys = unsortedKeys.sorted()
        
        var output = ""
        
        for key in sortedKeys {
            let keyValues:[Double] = arrCSV[key]!
            var line = "\(key),"
            var valueCounter = 0

            for value in keyValues {
                line = "\(line)\(value.as4PointString())"
                valueCounter += 1
                if valueCounter != keyValues.count {
                    line = "\(line),"
                }
            }

            output += line
            output += "\r\n"
        }

        if let outputAsData = output.data(using:.utf8, allowLossyConversion:true) {
            do {
                try outputAsData.write(to:pathForCSVWithExtension)
            }catch{
                print("X")
            }
        }
    }
    
    // called by Operation class when it wants to know if this operation is done
    override var isFinished: Bool
    {
        return self.bDone
    }

    // called by UI when the user wants to cancel this operation
    override func cancel()
    {
        self.bCancel = true
    }
}
