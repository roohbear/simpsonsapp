//
//  Extensions.swift
//  SimpsonsApp
//
//  Created by G Bear on 2019-12-20.
//  Copyright © 2019 RoohBear. All rights reserved.
//

import Foundation

extension Double
{
    func as4PointString() -> String
    {
        let str = String.init(format: "%.4f", self)
        return str
    }
}

